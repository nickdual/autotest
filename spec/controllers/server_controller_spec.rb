require 'spec_helper'

describe ServerController do
  describe "get api server" do
    it "response not nil with valid api-key" do
      conn = Faraday.new(:url => 'http://127.0.0.1:4000')
      response = conn.get '/server/restful_api' do |request|
        request.headers['Content-Type'] = 'application/json'
        request.headers['API-KEY'] = '27n02i1990b'
      end
      get :restful_api, {}
      response.status.should be(200)
    end
    it "response nil with invalid api-key" do
      conn = Faraday.new(:url => 'http://127.0.0.1:4000')
      response = conn.get '/server/restful_api' do |request|
        request.headers['Content-Type'] = 'application/json'
        request.headers['API-KEY'] = '123sd'
      end
      get :restful_api, {}
      response.status.should be(401)
    end

  end


end